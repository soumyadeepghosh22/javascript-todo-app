"use strict";

//Get all data from source
const getTodos = function(){
    const storedData = localStorage.getItem('todos');
    const data = JSON.parse(storedData);
    return data ? data : [];
}

//Fetch all data into table
const todoData = document.querySelector('#data');
const loadTodos = function(){
    const todos = getTodos();
    if(todos && todos.length > 0){
        let html = '';
        todos.forEach(function({title,desc},i){
            html +=  `
                <tr>
                    <td>${i+1}</td>
                    <td>${title}</td>
                    <td>${desc}</td>
                    <td class="text-center">
                        <button type="button" class="btn btn-outline-secondary edit-btn" data-id="${i}" onclick="editTodo(this)"><i class="fas fa-pencil-alt"></i></button>&nbsp;
                        <button type="button" class="btn btn-outline-danger delete-btn" data-id="${i}" onclick="deleteTodo(this)"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
            `;
        });
        todoData.innerHTML = html;
    }else{
        todoData.innerHTML = '<tr class="text-center"><td colspan="4">No record found</td></tr>'
    }
}
loadTodos();

const addBtn = document.querySelector('#add-btn');
const editBtn = document.querySelectorAll('.edit-btn');
const deleteBtn = document.querySelectorAll('.delete-btn');
const cancelBtn = document.querySelector('#cancel-btn');
const todoForm = document.querySelector('#add-form');

//Form open
const formOpen = function(id){
    if(Number.isInteger(id)){
        const data = getTodos();
        const {title, desc} = data[id];
        document.querySelector('input[type="hidden"]').value = id;
        document.querySelector('#title').value = title;
        document.querySelector('#desc').value = desc;
    }
    todoForm.classList.remove('hide');
    addBtn.classList.add('disabled');
    editBtn.forEach(e => e.classList.add('disabled'));
    deleteBtn.forEach(e => e.classList.add('disabled'));
    addBtn.style.cursor = 'not-allowed';
}
addBtn.addEventListener('click', formOpen);

//Form close
const formClose = function(){
    todoForm.classList.add('hide');
    addBtn.classList.remove('disabled');
    editBtn.forEach(e => e.classList.remove('disabled'));
    deleteBtn.forEach(e => e.classList.remove('disabled'));
    document.querySelector('input[type="hidden"]').value = '';
    document.querySelector('#title').value = '';
    document.querySelector('#desc').value = '';
    addBtn.style.cursor = 'pointer';
}
cancelBtn.addEventListener('click', formClose);

const form = document.querySelector('form');

//Add or Edit Todo
form.addEventListener('submit', function(e){
    e.preventDefault();
    try{
        const formData = new FormData(this);
        const id = formData.get('id');
        const title = formData.get('title');
        const desc = formData.get('desc');
        let data = getTodos();
        const todo = {title,desc};
        const action = (id === '' || id === null) ? 'create' : 'update';
        console.log(id, action)
        if(action === 'create'){
            data.push(todo);
            alertMessage("success", "Todo created successfully");
        }else{
            data[id] = todo;
            alertMessage("success", "Todo updated successfully");
        }
        localStorage.setItem('todos', JSON.stringify(data));
        this.reset();
        formClose();
        loadTodos();
    }catch(err){
        alertMessage("error", "Something went wrong");
    }
});

//Fetch existing data to form for edit
const editTodo = function(e){
    const index = parseInt(e.getAttribute('data-id'));
    formOpen(index)
}

//Delete Todo
const deleteTodo = function(e){
    const popup = confirm('Are you want to remove?')
    if(popup){
        const index = parseInt(e.getAttribute('data-id'));
        const data = getTodos();
        if(data.splice(index,1)){
            localStorage.setItem('todos', JSON.stringify(data));
            alertMessage("warning", "Todo deleted successfully");
            loadTodos();
        }else{
            alertMessage("error", "Something is going wrong");
        }
    }
}

//Alert message
const alertMessage = function(type,message){
    const messageDiv = document.querySelector('#message');
    let className = '';
    switch(type){
        case "success":
            className = 'alert-success';
            break;
        case "error":
            className = 'alert-danger';
            break;
        case "warning":
            className = 'alert-warning';
            break;
        default:
            className = 'alert-info';
    }
    messageDiv.classList.add('alert',className);
    messageDiv.innerHTML = message;
    setTimeout(() => {
        messageDiv.classList.remove('alert',className);
        messageDiv.innerHTML = '';
    }, 3000);
}